<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html lang= "en">
<head>
<meta charset="ISO-8859-1">
<title>Operation on Question Library Done Successful</title>
<style>
	body {
			background-color: #f5f5f5;
			font-family: Arial, sans-serif;
			color: #333;
			padding: 0;
			margin: 0;
		}
	h1 {
		color:green;
	}
	
	table {
		border-collapse: collapse;
		margin: 20px auto;
	}
	
	td {
		padding: 10px;
		border: 1px solid #2196F3;
	}
	
	a {
			display: inline-block;
			padding: 12px 24px;
			background-color: #0066cc;
			color: #fff;
			border-radius: 5px;
			text-decoration: none;
			margin: 20px 10px;
			font-size: 20px;
			transition: all 0.2s ease;
		}
	a:hover {
			background-color: #004080;
			transform: translateY(-2px);
			box-shadow: 0px 3px 6px rgba(0,0,0,0.2);
		}
</style>
</head>
<body>
	<div align="center">
		<h1>Operation on Question Library Done Successfully!!</h1>
		${message}
		<table>
			<tr>
				<td>Question ID:</td>
				<td>${question.questionId}</td>
			</tr>
			<tr>
				<td>Question Title:</td>
				<td>${question.questionTitle}</td>
			</tr>
			<tr>
				<td>Question Options:</td>
				<td>${question.options}</td>
			</tr>
			<tr>
				<td>Question Answer:</td>
				<td>${question.answer}</td>
			</tr>
			<tr>
				<td>Difficulty Level:</td>
				<td>${question.difficultLevel}</td>
			</tr>
			<tr>
				<td>Tagging Topic:</td>
				<td>${question.taggingTopic}</td>
			</tr>
		</table>
		<a href="viewQuestions">Back</a>
	</div>
</body>
</html>
