<%@ page language="java" import="java.util.List" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang= "en">
<head>
<meta charset="ISO-8859-1">
<title>Update Question</title>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
	$(document)
			.ready(
					function() {
						var max_fields = 5;
						var wrapper = $("#textbox-container");
						var add_button = $("#add-button");
						var x = 1;
						$(add_button)
								.click(
										function(e) {
											e.preventDefault();
											if (x < max_fields) {
												x++;
												$(wrapper)
														.append(
																'<div><input type="text" name="options"/><a href="#" class="remove-field">RemoveOption</a></div>');
											}
										});
						$(wrapper).on("click", ".remove-field", function(e) {
							e.preventDefault();
							$(this).parent('div').remove();
							x--;
						})
					});
</script>
<style>
body {
	background-color: #f2f2f2;
	font-family: Arial, sans-serif;
	margin: 0;
	padding: 0;
}

h1 {
	text-align: center;
	color: #333;
}

form {
	max-width: 500px;
	margin: 50px auto;
	padding: 20px;
	background-color: #fff;
	border-radius: 5px;
	box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
}

label {
	display: block;
	margin-bottom: 10px;
	font-size: 18px;
	color: #666;
}

input[type="text"], input[type="password"], input[type="number"], input[type="submit"]
	{
	display: block;
	width: 100%;
	padding: 10px;
	font-size: 18px;
	line-height: 1.5;
	color: #666;
	border: 1px solid #ccc;
	border-radius: 5px;
	margin-bottom: 20px;
	box-sizing: border-box;
	background-color: #f2f2f2;
}

input[type="submit"] {
	display: block;
	width: 100%;
	padding: 10px;
	background-color: #2196F3;
	color: #fff;
	font-size: 18px;
	line-height: 1.5;
	text-align: center;
	text-decoration: none;
	border-radius: 5px;
	cursor: pointer;
	transition: background-color 0.2s;
}

input[type="submit"]:hover {
	background-color: #0D47A1;
}

.message {
	color: red;
	margin: 10px 0;
	font-size: 18px;
	line-height: 1.5;
	text-align: center;
}

/* New style for options */
#textbox-container {
	background-color: #f2f2f2;
	padding: 10px;
	border: 1px solid #ccc;
	border-radius: 5px;
	margin-bottom: 20px;
}

#textbox-container input[type="text"] {
	display: inline-block;
	width: 80%;
	padding: 10px;
	font-size: 18px;
	line-height: 1.5;
	color: #666;
	border: 1px solid #ccc;
	border-radius: 5px;
	box-sizing: border-box;
	margin-bottom: 10px;
}

#add-button {
	display: inline-block;
	padding: 10px;
	background-color: #2196F3;
	color: #fff;
	font-size: 18px;
	line-height: 1.5;
	text-align: center;
	text-decoration: none;
	border-radius: 5px;
	cursor: pointer;
	transition: background-color
}

#difficultLevel {
	background-color: #f2f2f2; /* Set background color */
	color: #000000; /* Set text color */
	padding: 10px 20px; /* Add padding */
	font-size: 16px; /* Set font size */
	border: none; /* Remove border */
	border-radius: 4px; /* Add rounded corners */
	box-shadow: none; /* Remove box shadow */
}

/* Style the dropdown menu arrow */
#difficultLevel:after {
	content: "\25BC"; /* Add arrow symbol */
	position: absolute; /* Position arrow */
	top: 50%; /* Align arrow vertically */
	right: 10px; /* Align arrow horizontally */
	transform: translateY(-50%); /* Center arrow vertically */
	color: #000000; /* Set arrow color */
}

/* Style dropdown menu options */
#difficultLevel option {
	background-color: #f2f2f2; /* Set background color */
	color: #000000; /* Set text color */
	padding: 10px 20px; /* Add padding */
	font-size: 16px; /* Set font size */
	border: none; /* Remove border */
	border-radius: 4px; /* Add rounded corners */
}
</style>
</head>
<body>
<h1>Update Question</h1>
	<form action="updateQuestion" method="POST">
	<input type="hidden" name="questionId" value="${question.questionId}" />
	<label for="questionTitle">Enter updated details Question Text:</label> 
	<input type="text" id="questionTitle" name="questionTitle" value="${question.questionTitle}" required><br>
	<label for="options">Answer Options:</label>
	<div id="textbox-container">
			<c:forEach items="${question.options}" var="option">
			   <div>
			   <input type="text" id="options" name="options" value="${option}"><br>
			   <a href="#" class="remove-field">RemoveOption</a><br>
			   </div>	
			</c:forEach>
		</div>
		<button id="add-button">Add options</button>
    <label for="answer">Correct option:</label>
    <input type="number" id="answer" name="answer" value="${question.answer}" required><br>
    <select  name="difficultLevel" id="difficultLevel" value="${question.difficultLevel}" required="required">
    <option value="easy">easy</option>
    <option value="medium">medium</option>
    <option value="hard">hard</option>
    </select><br>
    <label for="Tagging topic">Topic:</label>
    <input type="text" id="taggingTopic" name="taggingTopic" value="${question.taggingTopic}" required><br>
    <input type="submit" value="modify">
	</form>
</body>
</html>