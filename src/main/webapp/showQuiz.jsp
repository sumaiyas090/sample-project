<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body align="center">
Quiz id : ${quiz.id}<br>
Quiz title:${quiz.title}<br>
(question weightage:${quiz.questionWeightage}, total Marks : ${quiz.totalmarks})<br>
Questions Available in quiz:<br>
<c:forEach items="${quiz.questions}" var="question">
Question id : ${question.questionId}<br>
Question title : ${question.questionTitle}<br>
Question options : ${question.options}<br>
Question answer : ${question.answer}<br>
difficulty level: ${question.difficultLevel}<br>
tagging topic : ${question.taggingTopic}
</c:forEach>

</body>
</html>