<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang= "en">
<head>
<meta charset="ISO-8859-1">
<title>view quiz</title>
<style>
  table {
    border-collapse: collapse;
    width: 100%;
  }
  body {
	background-color: #f9f9f9;
	font-family: Arial, sans-serif;
	margin: 0;
	padding: 0;
  }
th, td {
text-align: left;
padding: 8px;
}

tr:nth-child(even) {
background-color: #f2f2f2;
}

th {
background-color:#005daa;
color: white;
}
a {
display: inline-block;
padding: 10px;
background-color: #005daa;
color: #fff;
text-decoration: none;
border-radius: 5px;
margin: 10px;
}

a:hover {
	background-color: #004080;
    transform: translateY(-2px);
	box-shadow: 0px 3px 6px rgba(0,0,0,0.2);
}

h3 {
color: #005daa;
text-align: center;
}

.caption {
background-color: #005daa;
color: white;
padding: 10px;
margin-bottom: 20px;
border-radius: 5px;
}

.btn {
background-color: #d81c25;
color: white;
border: none;
padding: 10px 20px;
border-radius: 5px;
margin: 10px;
font-weight: bold;
}

.btn:hover {
    background-color: #004080;
    transform: translateY(-2px);
	box-shadow: 0px 3px 6px rgba(0,0,0,0.2);
}

.container {
max-width: 800px;
margin: 0 auto;
padding: 20px;
border-radius: 5px;
background-color: white;
box-shadow: 0px 0px 5px #ccc;
}
.delete {
	background-color: red;
	color: #fff;
  }
  .view {
	background-color: green;
	color: #fff;
  }
  .modify {
	background-color: orange;
	color: #000;
  }
  
</style>
</head>
<body>
<body>
<h3>Quiz Available</h3>
<h3>${message}</h3>
<a href="loginSuccess.jsp">Back</a>
<a href="getQuestions">create</a>
<table>
<caption></caption>
<tr>
<th>Id</th>
<th>QuizTitle</th>
<th>questions</th>
<th>QuestionWeightage</th>
<th>Totalmarks</th>
<th>Action</th>
</tr>
<c:forEach items="${quizzes}" var="quiz">
<tr>
<td><c:out value="${quiz.id}"/></td>
<td><c:out value="${quiz.title}"/></td>
<td>
<c:forEach items="${quiz.questions}" var="questions">
<c:out value="${questions.questionId}"/>
</c:forEach>
</td>
<td><c:out value="${quiz.questionWeightage}"/></td>
<td><c:out value="${quiz.totalmarks}"/></td>
<td><a href="deleteQuiz?id=${quiz.id}" class="delete">Delete</a> <a href="modifyQuiz?id=${quiz.id}" class="modify">Modify
</a><a href="viewQuizById?id=${quiz.id}" class="view">view</a>
</td>
</tr>
</c:forEach>
</table>
</body>
</html>