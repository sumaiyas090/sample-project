<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang= "en">
<head>
<meta charset="ISO-8859-1">
<title>Create Quiz</title>
<style>
		body {
			font-family: Arial, sans-serif;
			background-color: #f2f2f2;
		}
		h1 {
			text-align: center;
			color: #008CBA;
			margin-top: 50px;
			margin-bottom: 30px;
		}
		form {
			width: 50%;
			margin: auto;
			background-color: #fff;
			padding: 20px;
			border-radius: 10px;
			box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.2);
		}
		label {
			display: block;
			font-weight: bold;
			margin-bottom: 10px;
			color: #333;
		}
		input[type="text"],
		input[type="number"] {
			padding: 10px;
			border-radius: 5px;
			border: 1px solid #ccc;
			width: 100%;
			margin-bottom: 20px;
			box-sizing: border-box;
		}
		input[type="submit"] {
			background-color: #008CBA;
			color: #fff;
			border: none;
			padding: 10px 20px;
			border-radius: 5px;
			cursor: pointer;
		}
		input[type="submit"]:hover {
			background-color: #666;
		}
		table {
			border-collapse: collapse;
			width: 100%;
		}
		td {
			padding: 10px;
			border: 1px solid #ccc;
		}
		input[type="checkbox"] {
			margin-bottom: 10px;
		}
	</style>
</head>
<body align="center">
	<h1>Create Quiz</h1>
	<form action="updateQuiz" method="POST">
	<input type="hidden" name="id" value="${quiz.id}" />
	<label for="title">Quiz Title:</label> 
	<input type="text" id="title" name="title" value="${quiz.title}" required><br>
	<label for="questionWeightage">Weightage of each Question:</label>
	<input type="number" id="questionWeightage" name="questionWeightage" value="${quiz.questionWeightage}" required><br>
	<label>Select Questions from available questions</label>
   <table>
		<c:forEach items="${questions}" var="question">
			<tr>
				<td>
					<input type="checkbox" name="questionIdList" value="${question.questionId}"
					${quiz.questions.contains(question)? "checked": ""}>
				</td>
				<td>
					${question.questionId} 
				</td>
				<td>
				${question.questionTitle}
				</td>
			</tr>
		</c:forEach>
	</table>
    <input type="submit" value="Modify">
    </form>
</body>
</html>