<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang= "en">
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
.error-message {
  color: red;
  border: 1px solid red;
  padding: 10px;
  margin-bottom: 10px;
}
a {
			display: inline-block;
			padding: 12px 24px;
			background-color: #0066cc;
			color: #fff;
			border-radius: 5px;
			text-decoration: none;
			margin: 20px 10px;
			font-size: 20px;
			transition: all 0.2s ease;
		}
	a:hover {
			background-color: #004080;
			transform: translateY(-2px);
			box-shadow: 0px 3px 6px rgba(0,0,0,0.2);
		}
</style>
</head>
<body>
<h3 class="error-message">${message}</h3>
<a href="viewQuizzes">Back</a>
</body>
</html>