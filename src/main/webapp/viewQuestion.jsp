<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang= "en">
<head>
<meta charset="ISO-8859-1">
<title>View Question</title>
<style>
  table {
    border-collapse: collapse;
    width: 100%;
  }
  body {
	background-color: #f9f9f9;
	font-family: Arial, sans-serif;
	margin: 0;
	padding: 0;
  }
th, td {
text-align: left;
padding: 8px;
}

tr:nth-child(even) {
background-color: #f2f2f2;
}

th {
background-color:#005daa;
color: white;
}
a {
display: inline-block;
padding: 10px;
background-color: #005daa;
color: #fff;
text-decoration: none;
border-radius: 5px;
margin: 10px;
}

a:hover {
	background-color: #004080;
    transform: translateY(-2px);
	box-shadow: 0px 3px 6px rgba(0,0,0,0.2);
}

h3 {
color: #005daa;
text-align: center;
}

.caption {
background-color: #005daa;
color: white;
padding: 10px;
margin-bottom: 20px;
border-radius: 5px;
}

.btn {
background-color: #d81c25;
color: white;
border: none;
padding: 10px 20px;
border-radius: 5px;
margin: 10px;
font-weight: bold;
}

.btn:hover {
    background-color: #004080;
    transform: translateY(-2px);
	box-shadow: 0px 3px 6px rgba(0,0,0,0.2);
}

.container {
max-width: 800px;
margin: 0 auto;
padding: 20px;
border-radius: 5px;
background-color: white;
box-shadow: 0px 0px 5px #ccc;
}
.delete {
	background-color: red;
	color: #fff;
  }
  .view {
	background-color: green;
	color: #fff;
  }
  .modify {
	background-color: orange;
	color: #000;
  }
  
</style>
</head>
<body>
<h1 style="font-size: 18px; color: green; align:center">${message}</h1>
<h3 >Questions Available</h3>
<a href="loginSuccess.jsp">Back</a>
<a href="createQuestion.jsp">create</a>
<table>
<caption></caption>
<tr>

<th>Id</th>
<th>Question</th>
<th>Options</th>
<th>Difficulty</th>
<th>Tag</th>
<th>Answer</th>
<th>Action</th>

</tr>

<c:forEach items="${questions}" var="question">
<tr>
<td><c:out value="${question.questionId}"/></td>
<td><c:out value="${question.questionTitle}"/></td>
<td><c:out value="${question.options}"/></td>
<td><c:out value="${question.difficultLevel}"/></td>
<td><c:out value="${question.taggingTopic}"/></td>
<td><c:out value="${question.answer}"/></td>
<td>
<a href="deleteQuestion?questionId=${question.questionId}" class="delete">Delete</a> 
<a href="viewQuestionById?questionId=${question.questionId}" class="view">View</a>
<a href="modifyQuestion?questionId=${question.questionId}" class="modify">Modify</a>

</td>
</tr>
</c:forEach>
</table>

</body>

</html> 
