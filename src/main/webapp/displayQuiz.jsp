<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang= "en">
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
table {
    border-collapse: collapse;
    width: 100%;
  }
  body {
	background-color: #f2f2f2;
	font-family: Arial, sans-serif;
	margin: 0;
	padding: 0;
  }
  
  th, td {
    text-align: left;
    padding: 8px;
  }

  tr:nth-child(even) {
    background-color: #f2f2f2;
  }

  th {
    background-color: #666;
    color: white;
  }
</style>
</head>
<body>
<table border=1>
<caption>Available quizzes in Library</caption>
<tr>
<th>Id</th>
<th>Title</th>
<th>Marks Per Question</th>
<th>TotalMarks</th>
<th>Attempt</th>
</tr>
<c:forEach items="${quizzes}" var="quiz">
<tr>
<td><c:out value="${quiz.id}"/></td>
<td><c:out value="${quiz.title}"/></td>
<td><c:out value="${quiz.questionWeightage}"/></td>
<td><c:out value="${quiz.totalmarks}"/></td>
<td><a href="attemptQuiz?id=${quiz.id}">Attempt Quiz</a></td>
</tr>
</c:forEach>
</table>
</body>
</html>