<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang= "en">
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
body {
	background-color: #f2f2f2;
	font-family: Arial, sans-serif;
	margin: 0;
	padding: 0;
}

h1 {
	text-align: center;
	color: #333;
}

p {
	margin: 10px;
	font-size: 18px;
	line-height: 1.5;
	color: #666;
}

a {
	display: inline-block;
	padding: 10px;
	background-color: #333;
	color: #fff;
	text-decoration: none;
	border-radius: 5px;
	margin: 10px;
}

a:hover {
	background-color: #666;
}
</style>

</head>
<body align="center">
${message}
<a href="getQuiz">Click here to take Quiz</a>

</body>
</html>