<style>
body {
	background-color: #f2f2f2;
	font-family: Arial, sans-serif;
	margin: 0;
	padding: 0;
}

.container {
    width: 80%;
    margin: 0 auto;
    border: 1px solid #ccc;
    border-radius: 5px;
    background-color: #fff;
    padding: 20px;
}

.header {
    text-align: center;
    color: #333;
    margin-bottom: 30px;
}

.section {
    margin-bottom: 20px;
}

.section-title {
    font-size: 24px;
    font-weight: bold;
    margin-bottom: 10px;
    color: #333;
}

.section-content {
    font-size: 18px;
    color: #666;
    margin-left: 20px;
}

.link {
    display: inline-block;
    padding: 10px;
    background-color: #0066cc;
    color: #fff;
    text-decoration: none;
    border-radius: 5px;
    margin: 10px;
}

.link:hover {
    background-color: #666;
}
</style>

<body align="center">
<div class="container">
    <div class="header">
        <h1>Login successful as Admin!!!</h1>
    </div>
    <div class="section">
        <div class="section-title">Choose admin functionality:</div>
        <div class="section-content">
            <p><a href="viewQuestions" class="link">Click here for question library</a></p>
            <p><a href="viewQuizzes" class="link">Click here for quiz library</a></p>
            <p><a href="login.jsp" class="link">logout</a></p>
        </div>
    </div>
</div>
</body>
