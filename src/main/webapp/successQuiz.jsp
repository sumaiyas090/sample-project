<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html lang= "en">
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
	body {
			background-color: #f5f5f5;
			font-family: Arial, sans-serif;
			color: #333;
			padding: 0;
			margin: 0;
		}
	h1 {
		color:green;
	}
	
	table {
		border-collapse: collapse;
		margin: 20px auto;
	}
	
	td {
		padding: 10px;
		border: 1px solid #2196F3;
	}
	
	a {
			display: inline-block;
			padding: 12px 24px;
			background-color: #0066cc;
			color: #fff;
			border-radius: 5px;
			text-decoration: none;
			margin: 20px 10px;
			font-size: 20px;
			transition: all 0.2s ease;
		}
	a:hover {
			background-color: #004080;
			transform: translateY(-2px);
			box-shadow: 0px 3px 6px rgba(0,0,0,0.2);
		}
</style>
</head>
<body>
	<div align="center">
		<h1>Operation on Quiz Library Done Successfully!!</h1>
		${message}
		<table>
			<tr>
				<td>Quiz ID:</td>
				<td>${quiz.id}</td>
			</tr>
			<tr>
				<td>Quiz Title:</td>
				<td>${quiz.title}</td>
			</tr>
			<tr>
				<td>Questions:</td>
				<td>${quiz.questions}</td>
			</tr>
			<tr>
				<td>Question Weightage:</td>
				<td>${quiz.questionWeightage}</td>
			</tr>
			<tr>
				<td>Total marks:</td>
				<td>${quiz.totalmarks}</td>
			</tr>
		</table>
	</div>
</body>
<a href="viewQuizzes">Back</a>

</html>