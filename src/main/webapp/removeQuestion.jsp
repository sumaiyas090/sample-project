<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang= "en">
<head>
<meta charset="ISO-8859-1">
<title>Remove Question</title>
<style>
body {
	background-color: #f2f2f2;
	font-family: Arial, sans-serif;
	margin: 0;
	padding: 0;
}

h1 {
	text-align: center;
	color: #333;
}

form {
	max-width: 500px;
	margin: 50px auto;
	padding: 20px;
	background-color: #fff;
	border-radius: 5px;
	box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
}

label {
	display: block;
	margin-bottom: 10px;
	font-size: 18px;
	color: #666;
}

input[type="text"], input[type="password"] {
	display: block;
	width: 100%;
	padding: 10px;
	font-size: 18px;
	line-height: 1.5;
	color: #666;
	border: 1px solid #ccc;
	border-radius: 5px;
	margin-bottom: 20px;
	box-sizing: border-box;
}

input[type="radio"] {
	margin-right: 10px;
}

input[type="submit"] {
	display: block;
	width: 100%;
	padding: 10px;
	background-color: #333;
	color: #fff;
	font-size: 18px;
	line-height: 1.5;
	text-align: center;
	text-decoration: none;
	border-radius: 5px;
	cursor: pointer;
	transition: background-color 0.2s;
}

input[type="submit"]:hover {
	background-color: #666;
}

.message {
	color: red;
	margin: 10px 0;
	font-size: 18px;
	line-height: 1.5;
	text-align: center;
}
</style>
</head>
<body>
${message}
	<h1>Remove Question</h1>
	<form action="deleteQuestion" method="POST">
	<label for="questionId">Question ID:</label> 
	<input type="number" id="questionId" name="questionId" required><br>
	<input type="submit" value="remove">
</body>
</html>