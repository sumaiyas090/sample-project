<!DOCTYPE html>
<html>
<head>
	<title>Online Quiz Application</title>
	<style>
		body {
			background-color: #f5f5f5;
			font-family: Arial, sans-serif;
			color: #333;
			padding: 0;
			margin: 0;
		}

		header {
			background-color: #fff;
			box-shadow: 0px 3px 6px rgba(0,0,0,0.1);
			padding: 20px;
		}

		h1 {
			font-size: 36px;
			margin: 0;
			color: #333;
			font-weight: bold;
			text-align: center;
		}

		.container {
			max-width: 800px;
			margin: 0 auto;
			padding: 50px 20px;
			text-align: center;
			background-color: #fff;
			box-shadow: 0px 3px 6px rgba(0,0,0,0.1);
			border-radius: 10px;
			margin-top: 50px;
			margin-bottom: 50px;
		}

		p {
			font-size: 20px;
			margin-bottom: 20px;
			color: #666;
			line-height: 1.5;
			max-width: 600px;
			margin: 0 auto;
			padding: 0 20px;
		}

		a {
			display: inline-block;
			padding: 12px 24px;
			background-color: #0066cc;
			color: #fff;
			border-radius: 5px;
			text-decoration: none;
			margin: 20px 10px;
			font-size: 20px;
			transition: all 0.2s ease;
		}

		a:hover {
			background-color: #004080;
			transform: translateY(-2px);
			box-shadow: 0px 3px 6px rgba(0,0,0,0.2);
		}
	</style>
</head>
<body>
	<header>
		<h1>Welcome to the Online Quiz Application!!!</h1>
	</header>
	<div class="container">
		<p>If you already have an account, click the button below to log in:</p>
		<a href="login.jsp">Log in</a>
		<p>If you are a new user, click the button below to sign up:</p>
		<a href="signup.jsp">Sign up</a>
	</div>
</body>
</html>
