<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html lang= "en">
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
table {
    border-collapse: collapse;
    width: 100%;
  }
  body {
	background-color: #f2f2f2;
	font-family: Arial, sans-serif;
	margin: 0;
	padding: 0;
  }
  
  th, td {
    text-align: left;
    padding: 8px;
  }

  tr:nth-child(even) {
    background-color: #f2f2f2;
  }

  th {
    background-color: #666;
    color: white;
  }
</style>
</head>
<body>
<form action="displayResult" method="post">
<input type="hidden" name="id" value="${id}" />
<c:forEach items="${questions}" var="question">
<h4>${question.questionId})  ${question.questionTitle} "difficulty Level:"${question.difficultLevel}</h4>
<select  name="options" id="options" required="required">
<c:forEach items="${question.options}" var="option">
<option value="${option}">${option}</option>    
</c:forEach>

</select>

</c:forEach>

<input type="submit" value="submit">

</form>
</body>
</html>