package com.epam;
import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import java.util.*;

import com.epam.config.RsaKeyProperties;
import com.epam.databaselayer.UserRepository;
import com.epam.entities.User;


@SpringBootApplication
@EnableConfigurationProperties(RsaKeyProperties.class)
public class WebApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebApplication.class, args);
	}
	
	@Bean
	public ModelMapper getModelMapper() {
		return new ModelMapper();
	}
	
	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
//	@Bean
//	CommandLineRunner commandLineRunner(UserRepository users, PasswordEncoder encoder){
//	return args -> {
//	users.save(new User("sumi","user@epam.com", encoder.encode("User@123"),List.of("") ));
//	users.save(new User("sumi","sumi@epam.com","ROLE_USER,ROLE_ADMIN",encoder.encode("Sumi@123")));
//	};
//	}
}
