package com.epam.databaselayer;
import org.springframework.data.repository.CrudRepository;

import com.epam.entities.Question;
public interface QuestionRepository extends CrudRepository<Question, Integer>{
	    boolean existsByQuestionTitle(String questionTitle);
}
