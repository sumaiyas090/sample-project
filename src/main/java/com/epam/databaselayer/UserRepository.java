package com.epam.databaselayer;



import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.epam.entities.User;

public interface UserRepository extends CrudRepository<User,Integer>{
	//boolean existsByEmailAndPasswordAndRole(String email,String password,String role);
	boolean existsByEmail(String email);
	Optional<User> findByEmail(String email);
}
