package com.epam.databaselayer;


import org.springframework.data.repository.CrudRepository;

import com.epam.entities.Quiz;

public interface QuizRepository extends CrudRepository<Quiz, Integer> {
	 boolean existsByTitle(String title);
}
