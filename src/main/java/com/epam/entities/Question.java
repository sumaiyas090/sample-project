package com.epam.entities;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Table(name="question")
@Entity
public class Question {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int questionId;
	@NonNull
    private String questionTitle;
    @ElementCollection
    @NonNull
    private List<String> options;
    @ManyToMany(mappedBy = "questions",targetEntity = Quiz.class)
    @JsonIgnore
    List<Quiz> quiz;
    @NonNull
    private Integer answer;
    @NonNull
    private String difficultLevel;
    @NonNull
    private String taggingTopic;
   
    
	
}
