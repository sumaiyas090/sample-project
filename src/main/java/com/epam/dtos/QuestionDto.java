package com.epam.dtos;

import java.util.List;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class QuestionDto {
	 private int questionId;
	 @Size(min=3,max=30,message="Title should be 3 to 30 characters")
	 @NonNull
	 private String questionTitle;
	 @Size(min=2,max=10,message="There should be atleast two options")
	 @NonNull
	 private List<String> options;
	 @Min(value=1,message="Should be with in options limit")
	 @Max(value=10,message="Should be with in options limit")
	 @NonNull
	 private Integer answer;
	 @Pattern(regexp = "^(?i)(easy|medium|hard)$",message="Difficulty level should be one among easy,medium and hard")
	 @NonNull
	 private String difficultLevel;
	 @NotBlank
	 @NonNull
	 private String taggingTopic;
	 


}
