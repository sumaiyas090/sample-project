package com.epam.dtos;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
	private String name;
	@Email
	private String email;
	@Pattern(regexp = "^(?i)(admin|user)$", message = "User type should be either admin or user")
	private String type;
	@NotBlank
	@Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@#$%^&+=*!])(?=\\S+$).{8,}$", message = "At least 8 characters long and Contains at least one uppercase letter and Contains at least one lowercase letter and Contains at least one digit and Contains at least one special character (e.g. !, @, #, $, %, ^, &, *)")
	private String password;


}
