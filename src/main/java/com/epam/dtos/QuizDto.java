package com.epam.dtos;

import java.util.List;

import com.epam.entities.Question;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class QuizDto {
	private int id;
	@NotBlank
	@Size(min=3,max=30,message="Title should be 3 to 30 characters")
	@NonNull
    private String title;
    private int totalmarks;
    @Min(value = 1,message="question weightage should not be negative")
    @NonNull
    private Integer questionWeightage;
    private List<Question> questions;
	
}
