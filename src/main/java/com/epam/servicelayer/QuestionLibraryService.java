package com.epam.servicelayer;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.epam.databaselayer.QuestionRepository;
import com.epam.dtos.QuestionDto;
import com.epam.entities.Question;
import com.epam.exceptions.QuestionException;

@Service
public class QuestionLibraryService {
	@Autowired
	QuestionRepository questionRepository;
	@Autowired
	ModelMapper modelMapper;

	static final Logger log = LogManager.getLogger(QuestionLibraryService.class);

	public QuestionDto addQuestion(QuestionDto questionDto) throws QuestionException {
		log.info("Entered QuestionLibraryService:addQuestion....");
		if (!questionRepository.existsByQuestionTitle(questionDto.getQuestionTitle())) {
			Question question = modelMapper.map(questionDto, Question.class);
			log.info("Question saved Successfully");
			return modelMapper.map(questionRepository.save(question), QuestionDto.class);
		} else {
			throw new QuestionException(
					"Question Title " + questionDto.getQuestionTitle() + " already exist, Try modifying the question");
		}
	}

	public void removeQuestion(int quesId) throws QuestionException {
		log.info("Entered into QuestionLibraryService:removeQuestion....");
		try {
			questionRepository.deleteById(quesId);
		} catch (DataIntegrityViolationException e) {
			throw new QuestionException("This Question cannot be deleted as it is linked to quiz");
		}
	}

	public List<QuestionDto> getAllQuestions() {
		log.info("Entered QuestionLibraryService:getAllQuestions()....");
		return ((List<Question>) questionRepository.findAll()).stream()
				.map(question -> modelMapper.map(question, QuestionDto.class)).toList();
	}

	public QuestionDto modifyQuestion(QuestionDto questionDto) throws QuestionException {
		log.info("Entered QuestionLibraryService:modifyQuestion....");
		Question question = modelMapper.map(questionDto, Question.class);
		Optional<Question> questionOptional = questionRepository.findById(question.getQuestionId());
		return questionOptional.map(question1 -> {
			log.info("Question modified Successfully");
			return modelMapper.map(questionRepository.save(question), QuestionDto.class);
		}).orElseThrow(() -> new QuestionException("Question No" + question.getQuestionId() + " doenNot exists"));
	}

	public QuestionDto getQuestionById(int quesNo) throws QuestionException {
		log.info("Entered QuestionLibraryService:getQuestionById....");
		Optional<Question> questionOptional = questionRepository.findById(quesNo);
		return questionOptional.map(question -> {
			log.info("Question retrieved successfully");
			return modelMapper.map(question, QuestionDto.class);
		}).orElseThrow(() -> new QuestionException("Question No " + quesNo + " doenNot exists"));
	}

}
