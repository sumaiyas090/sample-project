package com.epam.servicelayer;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.databaselayer.UserRepository;
import com.epam.dtos.UserDto;
import com.epam.entities.User;
import com.epam.exceptions.UserException;

@Service
public class UserService {
    @Autowired
   UserRepository userRepository;
    @Autowired
    QuizLibraryService quizLibraryService;
    @Autowired 
    ModelMapper modelMapper;
    private static final Logger log=LogManager.getLogger(UserService.class);
//    public boolean validate(String email, String password,String type){
//    	log.info("UserService:validate");
//    	return userRepository.existsByEmailAndPasswordAndRole(email, password,type);
//    }
   public UserDto setUser(UserDto userDto) throws UserException{
	  log.info("UserService:setUser");
	  User user= modelMapper.map(userDto, User.class);
	   if(!userRepository.existsByEmail(user.getEmail())){
		   log.info("User saved succesfully");
		   return modelMapper.map(userRepository.save(user),UserDto.class);
	   }
	   else{
		   throw new UserException("user already Exists try to login");
	  }

   }
  
 
}

