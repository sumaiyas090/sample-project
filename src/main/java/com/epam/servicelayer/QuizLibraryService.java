package com.epam.servicelayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.databaselayer.QuestionRepository;
import com.epam.databaselayer.QuizRepository;
import com.epam.dtos.QuizDto;
import com.epam.entities.Question;
import com.epam.entities.Quiz;
import com.epam.exceptions.QuizException;

@Service
public class QuizLibraryService {
	@Autowired
	QuizRepository quizRepository;
	@Autowired
	QuestionRepository questionRepository;
	@Autowired
	ModelMapper modelMapper;
	static final Logger log = LogManager.getLogger(QuizLibraryService.class);

	public QuizDto addQuiz(QuizDto quizDto) throws QuizException {
		log.info("Entered QuizLibraryService:addQuiz");
		Quiz quiz = modelMapper.map(quizDto, Quiz.class);
		if (!quizRepository.existsByTitle(quiz.getTitle())) {
			log.info("Quiz saved Successfully");
			return modelMapper.map(quizRepository.save(quiz), QuizDto.class);
		} else {
			throw new QuizException("Quiz title " + quiz.getTitle() + " already exists!!\n Try Modifying the quiz");
		}

	}

	public List<QuizDto> getAllQuizzes() {
		log.info("Entered QuizLibraryService:getAllQuizzes");
		return ((List<Quiz>) quizRepository.findAll()).stream().map(quiz -> modelMapper.map(quiz, QuizDto.class))
				.toList();
	}

	public QuizDto modifyQuiz(QuizDto quizDto) throws QuizException {
		log.info("Entered QuizLibraryService:modifyQuiz");
		Quiz quiz = modelMapper.map(quizDto, Quiz.class);
		Optional<Quiz> quizOptional = quizRepository.findById(quiz.getId());
		return quizOptional.map(quiz1 -> {
			log.info("Quiz modified Successfully");
			return modelMapper.map(quizRepository.save(quiz), QuizDto.class);
		}).orElseThrow(() -> new QuizException("Quiz No " + quiz.getId() + " doenNot exists"));
	}

	public void removeQuiz(int id) {
		log.info("Entered QuizLibraryService:removeQuiz");
		quizRepository.deleteById(id);
	}

	public QuizDto createQuiz(QuizDto quizDto, List<Integer> questionIdList) throws QuizException {
		log.info("Entered QuizLibraryService:createQuiz");
		Quiz quiz = modelMapper.map(quizDto, Quiz.class);
		List<Question> questions = new ArrayList<>();
		int totalMarks;
		for (Integer questionId : questionIdList) {
			Optional<Question> question = questionRepository.findById(questionId);
			if (question.isPresent())
				questions.add(question.get());
		}
		if (questions.isEmpty()) {
			throw new QuizException("Atleast there should be one question in the quiz");
		}
		quiz.setQuestions(questions);
		totalMarks = questions.size() * quiz.getQuestionWeightage();
		quiz.setTotalmarks(totalMarks);
		log.info("Quiz created successfully");
		return modelMapper.map(quiz, QuizDto.class);

	}

	public QuizDto getQuizById(int id) throws QuizException {
		log.info("Entered QuizLibraryService:getQuizById");
		Optional<Quiz> quizOptional = quizRepository.findById(id);
		return quizOptional.map(quiz -> {
			log.info("Quiz retrieved successfully");
			return modelMapper.map(quiz, QuizDto.class);
		}).orElseThrow(() -> new QuizException("Quiz with " + id + " not found"));
	}

}
