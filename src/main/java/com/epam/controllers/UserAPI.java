package com.epam.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dtos.UserDto;
import com.epam.exceptions.UserException;
import com.epam.servicelayer.UserService;

import jakarta.validation.Valid;

@RestController
@RequestMapping(value="users")
public class UserAPI {
	@Autowired
	UserService userService;
	private static final Logger log=LogManager.getLogger(UserAPI.class);
	@PostMapping("/signup")
	public ResponseEntity<UserDto> setUser(@RequestBody @Valid UserDto userdto ) throws UserException{
		log.info("Received post request for adding a User");
		return new ResponseEntity<>(userService.setUser(userdto),HttpStatus.CREATED);
	}
	
//	@PostMapping("/login")
//	public ResponseEntity<String> login(@RequestBody @Valid UserDto userdto){
//		log.info("Received post request for validating user");
//		if(userService.validate(userdto.getEmail(), userdto.getPassword(),userdto.getType())) {
//			return new ResponseEntity<>("Login successful",HttpStatus.OK);
//		}else {
//			return new ResponseEntity<>("Login Failed",HttpStatus.UNAUTHORIZED);
//		}
//		
//	}
}
