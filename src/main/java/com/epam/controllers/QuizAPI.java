package com.epam.controllers;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dtos.QuizDto;
import com.epam.exceptions.QuizException;
import com.epam.servicelayer.QuizLibraryService;

import jakarta.validation.Valid;

@RestController
@RequestMapping(value="quizes")
public class QuizAPI {
	@Autowired
	QuizLibraryService quizLibraryService;
	private static final Logger log=LogManager.getLogger(QuizAPI.class);
	@GetMapping
	public ResponseEntity<List<QuizDto>> getAllQuizs(){
		log.info("Received get request to retrieve all Quizzes");
		return new ResponseEntity<>(quizLibraryService.getAllQuizzes(),HttpStatus.OK);
	}
	@GetMapping("/{id}")
	public ResponseEntity<QuizDto> getQuizById(@PathVariable  int id) throws  QuizException{
		log.info("Received get request to retrieve quiz by id");
		return new ResponseEntity<>(quizLibraryService.getQuizById(id),HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<QuizDto> addQuiz(@RequestBody @Valid QuizDto quizDto,@RequestParam List<Integer> questionIdList) throws QuizException {
		log.info("Received post request for adding a quiz");
		return new ResponseEntity<>(quizLibraryService.addQuiz(quizLibraryService.createQuiz(quizDto,questionIdList)),HttpStatus.CREATED);
		
	
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteQuiz(@PathVariable int id) {
		log.info("Received delete request to delete a quiz using Id");
		quizLibraryService.removeQuiz(id);
		return new ResponseEntity<>("Quiz with id "+id+" deleted Sucessfully",HttpStatus.OK);
		 
	}
	@PutMapping
	public  ResponseEntity<QuizDto> modifyQuiz(@RequestBody QuizDto quizDto,@RequestParam List<Integer> questionIdList) throws QuizException{
		log.info("Received put request to modify a quiz");
		return new ResponseEntity<>(quizLibraryService.modifyQuiz(quizLibraryService.createQuiz(quizDto,questionIdList)),HttpStatus.OK);
		
	}
	


}
