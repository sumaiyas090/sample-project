package com.epam.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.dtos.QuizDto;
import com.epam.exceptions.QuizException;
import com.epam.servicelayer.QuestionLibraryService;
import com.epam.servicelayer.QuizLibraryService;

@Controller
public class QuizController {
	@Autowired
	QuizLibraryService quizLibraryService;
	@Autowired
	QuestionLibraryService questionLibraryService;

	@PostMapping("addQuiz")
	public ModelAndView addQuiz(QuizDto quizdto,@RequestParam("questionIdList") List<Integer> questionIdList){
		ModelAndView modelAndView = new ModelAndView();
		try {
			QuizDto quiz = quizLibraryService.createQuiz(quizdto, questionIdList);
			quizLibraryService.addQuiz(quiz);
			modelAndView.setViewName("successQuiz.jsp");
			modelAndView.addObject("quiz", quiz);
			modelAndView.addObject("message", "Quiz added successfully");
		} catch (QuizException e) {
			modelAndView.addObject("message", e.getMessage());
			modelAndView.setViewName("errorQuiz.jsp");
		}
		return modelAndView;
	}
	

	@GetMapping("deleteQuiz")
	public ModelAndView deleteQuiz(@RequestParam("id") int id) {
		ModelAndView modelAndView = new ModelAndView();
		quizLibraryService.removeQuiz(id);
		modelAndView.setViewName("viewQuizzes");
		modelAndView.addObject("message", "Quiz deleted successfully");
		return modelAndView;

	}

	@GetMapping("viewQuizzes")
	public ModelAndView getQuizzes() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("quizzes", quizLibraryService.getAllQuizzes());
		modelAndView.setViewName("viewQuiz.jsp");
		return modelAndView;
	}

	@PostMapping("updateQuiz")
	public ModelAndView updateQuiz(QuizDto quizdto,@RequestParam("questionIdList") List<Integer> questionIdList)  {
		ModelAndView modelAndView = new ModelAndView();
		try {
		QuizDto quiz = quizLibraryService.createQuiz(quizdto, questionIdList);
		quizLibraryService.modifyQuiz(quiz);
		modelAndView.setViewName("successQuiz.jsp");
		modelAndView.addObject("message", "Quiz updated successfully");
		modelAndView.addObject("quiz", quiz);
		}catch (QuizException e) {
			modelAndView.addObject("message",e.getMessage());
			modelAndView.setViewName("errorQuiz.jsp");
		}
		return modelAndView;

	}
	@GetMapping("getQuestions")
	public ModelAndView getQuestions() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("createQuiz.jsp");
		modelAndView.addObject("questions", questionLibraryService.getAllQuestions());
		return modelAndView;

	}
	@GetMapping("viewQuizById")
	public ModelAndView viewQuizById(int id) {
		ModelAndView modelAndView = new ModelAndView();
	    try {		
		    modelAndView.setViewName("showQuiz.jsp");
		    modelAndView.addObject("quiz",quizLibraryService.getQuizById(id));
	    }catch(QuizException e){
		modelAndView.addObject("message",e.getMessage());
		modelAndView.setViewName("viewQuiz.jsp");
	    }
	    return modelAndView;
	}
	@GetMapping("modifyQuiz")
	public ModelAndView modifyQuizToGetDetails(int id) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("modifyQuiz.jsp");
	    try {
			modelAndView.addObject("quiz",quizLibraryService.getQuizById(id));
			modelAndView.addObject("questions",questionLibraryService.getAllQuestions());
			
		} catch (QuizException e) {
			modelAndView.addObject("message",e.getMessage());
			modelAndView.setViewName("errorQuiz.jsp");
		}
	    return modelAndView;
	}
}
