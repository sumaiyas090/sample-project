package com.epam.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.dtos.QuestionDto;
import com.epam.exceptions.QuestionException;
import com.epam.servicelayer.QuestionLibraryService;

@Controller
public class QuestionController {
	@Autowired
	QuestionLibraryService questionLibraryService;
	

	@GetMapping("viewQuestions")
	public ModelAndView getQuestions() {
		ModelAndView mv=new ModelAndView();
		List<QuestionDto> questions=questionLibraryService.getAllQuestions();
		if(!questions.isEmpty()) {
			mv.addObject("questions", questions);
			mv.setViewName("viewQuestion.jsp");
		}else
		{
			mv.addObject("message", "No Questions in question bank");
			mv.setViewName("viewQuizzes");
		}
		return mv;
	}
	
	
	
	@PostMapping("addQuestion")
	public ModelAndView addQuestion(QuestionDto question) {
		ModelAndView modelAndView = new ModelAndView();
		try {
		   modelAndView.addObject("question",questionLibraryService.addQuestion(question));
	       modelAndView.setViewName("successQuestion.jsp");
	       modelAndView.addObject("message","Question add successfully");
	      
		}
		catch(QuestionException e) {
			modelAndView.addObject("message",e.getMessage());
			modelAndView.setViewName("errorQuestion.jsp");

		}
		return modelAndView;		
	}
	@GetMapping("viewQuestionById")
	public ModelAndView viewQuesById(int questionId) {
		ModelAndView modelAndView = new ModelAndView();
	    try {		
		    modelAndView.setViewName("showQues.jsp");
		    modelAndView.addObject("question",questionLibraryService.getQuestionById(questionId));
	    }catch(QuestionException e){
		modelAndView.addObject("message",e.getMessage());
		modelAndView.setViewName("viewQuestion.jsp");
	    }
	    return modelAndView;
	}
	
	

	@GetMapping("deleteQuestion")
	public ModelAndView deleteQuestion(@RequestParam("questionId") int questionId){
		ModelAndView modelAndView = new ModelAndView();
	    try {	
	    	questionLibraryService.removeQuestion(questionId);
		    modelAndView.addObject("message","Question "+ questionId + " deleted successfully");
		    modelAndView.setViewName("viewQuestions");
	    }catch(QuestionException e){
		modelAndView.addObject("message",e.getMessage());
		modelAndView.setViewName("errorQuestion.jsp");
	    }
	    return modelAndView;
	    

	}
	@GetMapping("modifyQuestion")
		public ModelAndView modifyQuestionToGetDetails(int questionId){
			ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName("modifyQuestion.jsp");
		    try {
				modelAndView.addObject("question",questionLibraryService.getQuestionById(questionId));
			} catch (QuestionException e) {
				modelAndView.addObject("message",e.getMessage());
				modelAndView.setViewName("errorQuestion.jsp");
			}
		    return modelAndView;

		}
	
	
	@PostMapping("updateQuestion")
	public ModelAndView updateQuestion(QuestionDto ques){
		ModelAndView modelAndView = new ModelAndView();
		 try {
			modelAndView.setViewName("successQuestion.jsp");
			modelAndView.addObject("question",questionLibraryService.modifyQuestion(ques));
			modelAndView.addObject("message","Question updated successfully");
			} catch (QuestionException e) {
				modelAndView.addObject("message",e.getMessage());
				modelAndView.setViewName("errorQuestion.jsp");
			}
	    return modelAndView;

	}

}


