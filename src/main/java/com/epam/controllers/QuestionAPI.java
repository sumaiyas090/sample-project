package com.epam.controllers;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dtos.QuestionDto;
import com.epam.exceptions.QuestionException;
import com.epam.servicelayer.QuestionLibraryService;

import jakarta.validation.Valid;

@RestController
@RequestMapping(value="questions")
public class QuestionAPI {
	@Autowired
	QuestionLibraryService questionLibraryService;
	private static final Logger log=LogManager.getLogger(QuestionAPI.class);
	@GetMapping
	public ResponseEntity<List<QuestionDto>> getAllQuestions(){
		log.info("Received get request to retrieve all questions");
		return new ResponseEntity<>( questionLibraryService.getAllQuestions(), HttpStatus.OK);
	}
	@GetMapping("/{questionId}")
	public ResponseEntity<QuestionDto> getQuestionById(@PathVariable int questionId) throws QuestionException{
		log.info("Received get request to retrieve question by id");
		return new ResponseEntity<>(questionLibraryService.getQuestionById(questionId), HttpStatus.OK);
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<QuestionDto> addQuestion(@RequestBody @Valid QuestionDto questionDto) throws QuestionException {
		log.info("Received post request for adding a question");
		return new ResponseEntity<>(questionLibraryService.addQuestion(questionDto), HttpStatus.CREATED);
	}
	@DeleteMapping("/{questionId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void  deleteQuestion(@PathVariable int questionId) throws QuestionException {
		log.info("Received delete request to delete a question using Id");
		questionLibraryService.removeQuestion(questionId);
	}
	@PutMapping
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<QuestionDto> modifyQuestion(@RequestBody @Valid QuestionDto questionDto) throws QuestionException{
		log.info("Received put request to modify a question");
		return new ResponseEntity<>(questionLibraryService.modifyQuestion(questionDto), HttpStatus.OK);
	}
	
	
	
}
