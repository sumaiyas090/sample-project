package com.epam.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.dtos.QuizDto;
import com.epam.dtos.UserDto;
import com.epam.entities.Question;
import com.epam.exceptions.QuizException;
import com.epam.exceptions.UserException;
import com.epam.servicelayer.QuizLibraryService;
import com.epam.servicelayer.UserService;

@Controller
public class UserController {
	@Autowired
	UserService userService;
	@Autowired
	QuizLibraryService quizLibraryService;


	@RequestMapping("/")
	public String homePage() {
		return "home.jsp";
	}
	
//	@PostMapping("validate")
//	public ModelAndView validate(String email,String password,String type){
//		ModelAndView modelAndView=new ModelAndView();
//		    if(userService.validate(email,password,type) && type.equals("admin")){
//			     modelAndView.setViewName("loginSuccess.jsp");
//		    }
//		    else if(userService.validate(email,password,type) && type.equals("user")){
//			     modelAndView.setViewName("userPage.jsp");}
//		    else {
//		    	modelAndView.addObject("message", "Login Failed!!\nTry Again!!!");
//				modelAndView.setViewName("login.jsp");
//		    }
//	
//		return modelAndView;
//	}
	
	@PostMapping("register")
	public ModelAndView setUser(UserDto user)  {
		ModelAndView modelAndView=new ModelAndView();
		user.setType("user");
		    try {
				modelAndView.addObject("message","Hello "+userService.setUser(user).getName()+" \nRegistrarion Successful!!!");
			} catch (UserException e) {
				modelAndView.addObject("message", e.getMessage());
			}
			modelAndView.setViewName("login.jsp");
		
		return modelAndView;
	}
	
	@GetMapping("getQuiz")            
	public ModelAndView getQuizzes() {
		ModelAndView modelAndView=new ModelAndView();
		List<QuizDto> quizList=quizLibraryService.getAllQuizzes();
		modelAndView.addObject("quizzes", quizList);
		modelAndView.setViewName("displayQuiz.jsp");
		return modelAndView;
	}
	@GetMapping("attemptQuiz")                  
	public ModelAndView takeQuiz(int id) {
		ModelAndView modelAndView=new ModelAndView();
		try {
			List<Question> questionsList=quizLibraryService.getQuizById(id).getQuestions();
			modelAndView.addObject("questions", questionsList);
			modelAndView.setViewName("displayQuestions.jsp");
			modelAndView.addObject("id", id);
 
		}catch(QuizException e) {
			modelAndView.addObject("message", e.getMessage());
			modelAndView.setViewName("error.jsp");
		}
		return modelAndView;
	}
	@PostMapping("displayResult")           
	public ModelAndView getResult(int id, @RequestParam("options") List<String> options) throws QuizException {
		ModelAndView modelAndView=new ModelAndView();
		List<Integer> answers=new ArrayList<>();
		int i=0;
		for(Question question: quizLibraryService.getQuizById(id).getQuestions()) {
			answers.add(question.getOptions().indexOf(options.get(i))+1);
			i++;
		}
		QuizDto quiz=quizLibraryService.getQuizById(id);
		List<Integer> correctAnswers=quiz.getQuestions().stream().map(Question::getAnswer).toList();
		long result=IntStream.range(0, correctAnswers.size()).filter(index->correctAnswers.get(index).equals(answers.get(index))).count()*quiz.getQuestionWeightage();
		modelAndView.addObject("result", result);
		modelAndView.setViewName("displayResult.jsp");
		return modelAndView;
	}

	
}
