package com.epam.exceptions;

@SuppressWarnings("serial")
public class QuestionException extends Exception{
	public QuestionException(String s) {
		super(s);
	}
}
