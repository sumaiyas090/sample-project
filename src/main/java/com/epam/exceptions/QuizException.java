package com.epam.exceptions;
@SuppressWarnings("serial")
public class QuizException extends Exception {
	public QuizException(String s) {
		super(s);
	}
}
