package com.epam.exceptions;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@RestControllerAdvice
public class ApplicationExceptionHandler {
	private static final Logger log=LogManager.getLogger(ApplicationExceptionHandler.class);
	@ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException e,WebRequest request) {
		List<String> error = new ArrayList<>();
		e.getAllErrors().forEach(err -> error.add(err.getDefaultMessage()));
		log.error("Encountered MethodArgumentNotValidException :{}",error);
		return new ExceptionResponse(new Date().toString(), 
				HttpStatus.BAD_REQUEST.toString(), 
				error.toString(),
				request.getDescription(false));
		

	}
	
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleMethodArgumentMismatchException(MethodArgumentTypeMismatchException e,WebRequest request) {	
		log.error("Encountered MethodArgumentMismatchException :{}",e.getMessage());
		return new ExceptionResponse(new Date().toString(), 
				HttpStatus.BAD_REQUEST.toString(), 
				e.getMessage(),
				request.getDescription(false));

		
	}
	@ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleHttpMessageNotReadableException(HttpMessageNotReadableException e,WebRequest request) {	
		log.error("Encountered HttpMessageNotReadableException :{}",e.getMessage());
		return new ExceptionResponse(new Date().toString(), 
				HttpStatus.BAD_REQUEST.toString(), 
				e.getMessage(),
				request.getDescription(false));

	}
	
	@ExceptionHandler(QuestionException.class)
	public ExceptionResponse handleMethodRedudantException(QuestionException e,WebRequest request) {	
		log.error("Encountered QuestionException :{}",e.getMessage());
		return new ExceptionResponse(new Date().toString(), 
				HttpStatus.OK.toString(), 
				e.getMessage(),
				request.getDescription(false));

	}
	@ExceptionHandler(QuizException.class)
	public ExceptionResponse handleOperationFailedException(QuizException e,WebRequest request) {	
		log.error("Encountered QuizException :{}",e.getMessage());
		return new ExceptionResponse(new Date().toString(), 
				HttpStatus.OK.toString(), 
				e.getMessage(),
				request.getDescription(false));

	}
	@ExceptionHandler(UserException.class)
	public ExceptionResponse handleElementNotFoundException(UserException e,WebRequest request) {	
		log.error("Encountered UserException :{}",e.getMessage());
		return new ExceptionResponse(new Date().toString(), 
				HttpStatus.OK.toString(), 
				e.getMessage(),
				request.getDescription(false));

	}
	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ExceptionResponse handleRuntimeException(RuntimeException e,WebRequest request) {	
		log.error("Encountered RuntimeException :{}",e.getMessage());
		return new ExceptionResponse(new Date().toString(), 
				HttpStatus.INTERNAL_SERVER_ERROR.toString(), 
				e.getMessage(),
				request.getDescription(false));

	}
	@ExceptionHandler(AccessDeniedException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public ExceptionResponse handleAccessDeniedException(RuntimeException e,WebRequest request) {	
		log.error("Encountered RuntimeException :{}",e.getMessage());
		return new ExceptionResponse(new Date().toString(), 
				HttpStatus.FORBIDDEN.toString(), 
				e.getMessage(),
				request.getDescription(false));

	}
	


}
