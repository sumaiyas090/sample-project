package com.epam.exceptions;
@SuppressWarnings("serial")
public class UserException extends Exception{
	  public UserException(String s) {
		  super(s);
	  }
}
