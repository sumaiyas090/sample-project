package com.epam.servicelayer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;

import com.epam.databaselayer.QuestionRepository;
import com.epam.dtos.QuestionDto;
import com.epam.entities.Question;
import com.epam.exceptions.QuestionException;


@ExtendWith(MockitoExtension.class)
 class QuestionLibraryServiceTest {
    @Mock
    QuestionRepository questionRepository;
    @Mock
    ModelMapper modelMapper;
    @InjectMocks
    QuestionLibraryService questionServiceImpl;
	Question question1,question;
    QuestionDto questionDto;
    @BeforeEach
    void setup(){
    	questionDto=new QuestionDto("Capital of TN", Arrays.asList("chennai","Hyd"),1,"easy","gk");
        question=new Question("Capital of TN", Arrays.asList("chennai","Hyd"),1,"easy","gk");
        question1=new Question("2>0",Arrays.asList("true","false"),1,"easy","maths");
    }
    
    @Test
    void testAddQuestion() throws QuestionException {
    	Mockito.when(modelMapper.map(questionDto, Question.class)).thenReturn(question);
        Mockito.when(questionRepository.save(question)).thenReturn(question);
        Mockito.when(modelMapper.map(question, QuestionDto.class)).thenReturn(questionDto);
        assertEquals(questionDto,questionServiceImpl.addQuestion(questionDto));
        Mockito.verify(questionRepository).save(question);
    }
    
    
    @Test
    void testAddQuestionWithException(){
        Mockito.when(questionRepository.existsByQuestionTitle("Capital of TN")).thenReturn(true);
        assertThrows(QuestionException.class,()->questionServiceImpl.addQuestion(questionDto));
       
    }
    @Test
    void testRemoveQuestion() throws QuestionException {
        Mockito.doNothing().when(questionRepository).deleteById(1);
        questionServiceImpl.removeQuestion(1);
 
    }
   
    @Test
    void testRemoveQuestionWithException() throws QuestionException {
    	Mockito.doThrow(DataIntegrityViolationException.class).when(questionRepository).deleteById(1);
    	assertThrows(QuestionException.class, ()->questionServiceImpl.removeQuestion(1));
    }
    
    @Test
    void testViewQuestion(){
        List<QuestionDto> questionsDto=Arrays.asList(questionDto);
        List<Question> questions=Arrays.asList(question);
        Mockito.when(questionRepository.findAll()).thenReturn(questions);
        Mockito.when(modelMapper.map(question, QuestionDto.class)).thenReturn(questionDto);
        List<QuestionDto> verifiedQuestions=questionServiceImpl.getAllQuestions();
        assertEquals(questionsDto,verifiedQuestions);
        
    }
  
    @Test
    void testModifyQuestion() throws QuestionException{
    	Mockito.when(modelMapper.map(questionDto, Question.class)).thenReturn(question);
    	Mockito.when(questionRepository.findById(0)).thenReturn(Optional.of(question));
        Mockito.when(questionRepository.save(question)).thenReturn(question);
        Mockito.when(modelMapper.map(question, QuestionDto.class)).thenReturn(questionDto);
        QuestionDto modifiedQuestion=questionServiceImpl.modifyQuestion(questionDto);
        assertEquals(modifiedQuestion,questionDto);
    }
    @Test
    void testModifyQuestionWithException() throws QuestionException{
    	Mockito.when(modelMapper.map(questionDto, Question.class)).thenReturn(question);
    	Mockito.when(questionRepository.findById(0)).thenReturn(Optional.empty());
    	assertThrows(QuestionException.class,()->questionServiceImpl.modifyQuestion(questionDto));
    }
   @Test
   void testViewQuestionById() throws QuestionException{
	   Mockito.when(questionRepository.findById(1)).thenReturn(Optional.of(question));
	   Mockito.when(modelMapper.map(question, QuestionDto.class)).thenReturn(questionDto);
	   QuestionDto result=questionServiceImpl.getQuestionById(1);
       assertEquals(result,questionDto);
       
   }
   @Test
   void testViewQuestionByIdEithException(){
	   Mockito.when(questionRepository.findById(1)).thenReturn(Optional.empty());
       assertThrows(QuestionException.class, ()->questionServiceImpl.getQuestionById(1));
   }

   
}
