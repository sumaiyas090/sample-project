package com.epam.servicelayer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.databaselayer.UserRepository;
import com.epam.dtos.UserDto;
import com.epam.entities.User;
import com.epam.exceptions.UserException;


@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    @Mock
    UserRepository userRepository;
    @Mock
    QuizLibraryService quizLibraryService;
    @Mock
    ModelMapper modelmapper;
    @InjectMocks
    UserService userService;
    User user;
    UserDto userDto;
    @BeforeEach
    void setup(){
        user=new User("sumi","sumi@gmail.com","sumi","user");
        userDto=new UserDto("sumi","sumi@gmail.com","sumi","user");
    }
    
    
    @Test
    void testValidity(){
        Mockito.when(userRepository.existsByEmailAndPasswordAndRole("sumi@gmail.com","sumi","admin")).thenReturn(true);
        boolean result=userService.validate("sumi@gmail.com","sumi","admin");
        assertTrue(result);
    }
    @Test
    void testValidityWithWrongPassword(){
    	 Mockito.when(userRepository.existsByEmailAndPasswordAndRole("sumi@gmail.com","sumi","admin")).thenReturn(false);
         boolean result=userService.validate("sumi@gmail.com","sumi","admin");
        assertFalse(result);
    }
    @Test
    void testValidityWithException()  {
        assertFalse(userService.validate("sum@gmail.com","sum","admin"));
    }
    @Test
    void testSetUser() throws UserException{
    	Mockito.when(modelmapper.map(userDto, User.class)).thenReturn(user);
    	Mockito.when(userRepository.existsByEmail("sumi@gmail.com")).thenReturn(false);
        Mockito.when(userRepository.save(user)).thenReturn(user);
        Mockito.when(modelmapper.map(userRepository.save(user),UserDto.class)).thenReturn(userDto);
        assertEquals(userDto,userService.setUser(userDto));
  
    }
    @Test
    void testSetUserWithException(){
    	Mockito.when(modelmapper.map(userDto, User.class)).thenReturn(user);
    	Mockito.when(userRepository.existsByEmail("sumi@gmail.com")).thenReturn(true);
        assertThrows(UserException.class,()->userService.setUser(userDto));
    }
    
    
    
}
