package com.epam.servicelayer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.databaselayer.QuestionRepository;
import com.epam.databaselayer.QuizRepository;
import com.epam.dtos.QuizDto;
import com.epam.entities.Question;
import com.epam.entities.Quiz;
import com.epam.exceptions.QuizException;

@ExtendWith(MockitoExtension.class)
class QuizLibraryServiceTest {
	@Mock
	QuizRepository quizRepository;
	@Mock
	QuestionRepository questionRepository;
	@Mock
	ModelMapper modelMapper;
	@InjectMocks
	QuizLibraryService quizServiceImpl;

	Quiz quiz, quiz1;
	QuizDto quizDto;

	List<Question> gkQuestions, mathsQuestions;

	@BeforeEach
	void setup() {
		gkQuestions = Arrays.asList(new Question("Capital of TN", Arrays.asList("chennai", "Hyd"), 1, "easy", "gk"));
		mathsQuestions = Arrays.asList(new Question("2>0", Arrays.asList("true", "false"), 1, "easy", "maths"));
		quiz = new Quiz("GKQuiz", 2);
		quiz.setQuestions(gkQuestions);
		quizDto = new QuizDto("GkQuiz", 2);
		quizDto.setQuestions(gkQuestions);
	}

	@Test
	void testAddQuiz() throws QuizException {
		Mockito.when(quizRepository.existsByTitle("GKQuiz")).thenReturn(false);
		Mockito.when(modelMapper.map(quizDto, Quiz.class)).thenReturn(quiz);
		Mockito.when(quizRepository.save(quiz)).thenReturn(quiz);
		Mockito.when(modelMapper.map(quiz, QuizDto.class)).thenReturn(quizDto);
		QuizDto insertedQuiz = quizServiceImpl.addQuiz(quizDto);
		assertEquals(quizDto, insertedQuiz);
		Mockito.verify(quizRepository).existsByTitle("GKQuiz");
		Mockito.verify(quizRepository).save(quiz);

	}

	@Test
	void testAddQuizWithException() {
		Mockito.when(modelMapper.map(quizDto, Quiz.class)).thenReturn(quiz);
		Mockito.when(quizRepository.existsByTitle("GKQuiz")).thenReturn(true);
		assertThrows(QuizException.class, () -> quizServiceImpl.addQuiz(quizDto));
		Mockito.verify(quizRepository).existsByTitle("GKQuiz");
	}

	@Test
	void testRemoveQuiz() {
		Mockito.doNothing().when(quizRepository).deleteById(1);
		quizServiceImpl.removeQuiz(1);
	}
	


	@Test
	void testViewQuiz() {
		List<QuizDto> quizsDto = Arrays.asList(quizDto);
		List<Quiz> quizs = Arrays.asList(quiz);
		Mockito.when(quizRepository.findAll()).thenReturn(quizs);
		Mockito.when(modelMapper.map(quiz, QuizDto.class)).thenReturn(quizDto);
		List<QuizDto> verifiedQuizs = quizServiceImpl.getAllQuizzes();
		assertEquals(quizsDto, verifiedQuizs);
	}

	@Test
	void testModifyQuiz() throws QuizException {
		Mockito.when(modelMapper.map(quizDto, Quiz.class)).thenReturn(quiz);
		Mockito.when(quizRepository.findById(0)).thenReturn(Optional.of(quiz));
		Mockito.when(quizRepository.save(quiz)).thenReturn(quiz);
		Mockito.when(modelMapper.map(quiz, QuizDto.class)).thenReturn(quizDto);
		QuizDto modifiedQuiz = quizServiceImpl.modifyQuiz(quizDto);
		assertEquals(quizDto, modifiedQuiz);
		Mockito.verify(quizRepository).save(quiz);

	}

	@Test
	void testModifyQuizWithException() {
		Mockito.when(modelMapper.map(quizDto, Quiz.class)).thenReturn(quiz);
		Mockito.when(quizRepository.findById(0)).thenReturn(Optional.empty());
		assertThrows(QuizException.class, () -> quizServiceImpl.modifyQuiz(quizDto));

	}

	@Test
	void testCreateQuiz() throws  QuizException {
		Mockito.when(modelMapper.map(quizDto, Quiz.class)).thenReturn(quiz);
		Mockito.when(questionRepository.findById(1)).thenReturn(Optional.ofNullable(gkQuestions.get(0)));
		Mockito.when(modelMapper.map(quiz, QuizDto.class)).thenReturn(quizDto);
		assertEquals(quiz.getId(), quizServiceImpl.createQuiz(quizDto, Arrays.asList(1)).getId());
		Mockito.verify(questionRepository).findById(1);
	}

	@Test
	void testviewQuizById() throws QuizException {
		Mockito.when(quizRepository.findById(1)).thenReturn(Optional.of(quiz));
		Mockito.when(modelMapper.map(quiz, QuizDto.class)).thenReturn(quizDto);
		assertEquals(quizDto, quizServiceImpl.getQuizById(1));
	}

	@Test
	void testviewQuizById1() throws QuizException {
		Mockito.when(quizRepository.findById(1)).thenReturn(Optional.empty());
		assertThrows(QuizException.class, () -> quizServiceImpl.getQuizById(1));
	}

	@Test
    void addQuestionsToQuizFail(){
		Mockito.when(modelMapper.map(quizDto, Quiz.class)).thenReturn(quiz);
		Mockito.when(questionRepository.findById(0)).thenReturn(Optional.empty());
		assertThrows(QuizException.class,()->quizServiceImpl.createQuiz(quizDto,List.of(0)));
	}
}
