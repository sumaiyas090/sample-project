package com.epam.controllers;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.epam.dtos.QuizDto;
import com.epam.dtos.UserDto;
import com.epam.entities.Question;
import com.epam.exceptions.QuizException;
import com.epam.exceptions.UserException;
import com.epam.servicelayer.QuizLibraryService;
import com.epam.servicelayer.UserService;
@WebMvcTest(controllers=UserController.class,excludeAutoConfiguration= {SecurityAutoConfiguration.class})
 class UserControllerTest {
	@MockBean
	UserService userService;
	@MockBean
	QuizLibraryService quizLibraryService;
	@Autowired
	private MockMvc mockMvc;
	UserDto user;
	@BeforeEach
	public void setUp() {
		user=new UserDto("sumi","sumi@gmail.com","sumi","admin");
	}
	
	@Test
	void testValidateAdmin() throws Exception {
	
		Mockito.when(userService.validate("sumi@gmail.com","sumi","admin")).thenReturn(true);
		MvcResult mvcResult = mockMvc.perform(post("/validate")
				.param("email","sumi@gmail.com")
				.param("password","sumi")
				.param("type", "admin"))
				.andExpect(status().isOk())
				.andExpect(view().name("loginSuccess.jsp")).andReturn();
		assertNotNull(mvcResult);
	}
	@Test
	void testValidateUser() throws Exception {
		Mockito.when(userService.validate("sumi@gmail.com","sumi","user")).thenReturn(true);
		MvcResult mvcResult = mockMvc.perform(post("/validate")
				.param("email","sumi@gmail.com")
				.param("password","sumi")
				.param("type", "user"))
				.andExpect(status().isOk())
				.andExpect(view().name("userPage.jsp")).andReturn();
		assertNotNull(mvcResult);
	}
	@Test
	void testValidateUserWithException() throws Exception {
		Mockito.when(userService.validate(any(), any(),any())).thenReturn(false);
		MvcResult mvcResult = mockMvc.perform(post("/validate")
				.param("type", "user"))
				.andExpect(status().isOk())
				.andExpect(view().name("login.jsp"))
				.andReturn();
		assertNotNull(mvcResult);
	}
	@Test
	void testSetUser() throws Exception {
		Mockito.when(userService.setUser(any())).thenReturn(user);
		MvcResult mvcResult = mockMvc.perform(post("/register"))
				.andExpect(status().isOk())
				.andExpect(view().name("login.jsp"))
				.andReturn();
		assertNotNull(mvcResult);
	}
	@Test
	void testSetUserWithException() throws Exception {
		Mockito.when(userService.setUser(any())).thenThrow(new UserException("User already there"));
		MvcResult mvcResult = mockMvc.perform(post("/register"))
				.andExpect(status().isOk())
				.andExpect(model().attribute("message","User already there"))
				.andExpect(view().name("login.jsp"))
				.andReturn();
		assertNotNull(mvcResult);
	}
	
	@Test
    void testGetQuizzes() throws Exception {
    	List<Question> gkQuestions= Arrays.asList(new Question("Capital of TN",Arrays.asList("chennai","Hyd"),1,"easy","gk"));
    	QuizDto quiz=new QuizDto("GKQuiz",2);
    	quiz.setQuestions(gkQuestions);
    	Mockito.when(quizLibraryService.getAllQuizzes()).thenReturn(List.of(quiz));
    	MvcResult mvcResult = mockMvc.perform(get("/getQuiz"))
				.andExpect(status().isOk())
				.andExpect(model().attribute("quizzes",List.of(quiz)))
				.andExpect(view().name("displayQuiz.jsp"))
				.andReturn();
    	assertNotNull(mvcResult);
    }
	@Test
	void testAttemptQuiz() throws Exception {
		List<Question> gkQuestions= Arrays.asList(new Question("Capital of TN",Arrays.asList("chennai","Hyd"),1,"easy","gk"));
		QuizDto quiz=new QuizDto("GKQuiz",2);
		quiz.setQuestions(gkQuestions);
    	Mockito.when(quizLibraryService.getQuizById(1)).thenReturn(quiz);
    	MvcResult mvcResult = mockMvc.perform(get("/attemptQuiz")
    			.param("id","1"))
				.andExpect(status().isOk())
				.andExpect(view().name("displayQuestions.jsp"))
				.andReturn();
    	assertNotNull(mvcResult);
	}
	@Test
	void testAttemptQuizWithException() throws Exception {
    	Mockito.when(quizLibraryService.getQuizById(1)).thenThrow(new QuizException("error"));
    	MvcResult mvcResult = mockMvc.perform(get("/attemptQuiz")
    			.param("id","1"))
				.andExpect(status().isOk())
				.andExpect(view().name("error.jsp"))
				.andReturn();
    	assertNotNull(mvcResult);
	}
	
	@Test
	void testGetResults() throws Exception {
		List<Question> gkQuestions= Arrays.asList(new Question("Capital of TN",Arrays.asList("chennai","Hyd"),1,"easy","gk"));
		QuizDto quiz=new QuizDto("GKQuiz",2);
		quiz.setQuestions(gkQuestions);
		Mockito.when(quizLibraryService.getQuizById(1)).thenReturn(quiz);	
		MvcResult mvcResult = mockMvc.perform(post("/displayResult")
    			.param("id","1")
    			.param("options", "List.Of(1,2)"))
				.andExpect(status().isOk())
				.andExpect(view().name("displayResult.jsp"))
				.andReturn();
    	assertNotNull(mvcResult);
		
	}
    @Test
    void testDisplayHomePage() throws Exception {
    	MvcResult mvcResult = mockMvc.perform(get("/"))
    			.andExpect(status().isOk())
    			.andReturn();
    	assertNotNull(mvcResult);
    }

}
