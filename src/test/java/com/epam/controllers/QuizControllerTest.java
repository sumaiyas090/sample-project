package com.epam.controllers;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.epam.dtos.QuestionDto;
import com.epam.dtos.QuizDto;
import com.epam.entities.Question;
import com.epam.exceptions.QuizException;
import com.epam.servicelayer.QuestionLibraryService;
import com.epam.servicelayer.QuizLibraryService;
@WebMvcTest(controllers=QuizController.class,excludeAutoConfiguration= {SecurityAutoConfiguration.class})
 class QuizControllerTest {
	@MockBean
	QuizLibraryService quizLibraryService;
	@MockBean
	QuestionLibraryService questionLibraryService;
	@Autowired
	private MockMvc mockMvc;
	List<Question> gkQuestions;
	List<QuestionDto> questionsDto;
	QuizDto quiz;
	@BeforeEach
	public void setUp() {
		gkQuestions=Arrays.asList(new Question("Capital of TN",Arrays.asList("chennai","Hyd"),1,"easy","gk"));
		questionsDto=Arrays.asList(new QuestionDto("Capital of TN",Arrays.asList("chennai","Hyd"),1,"easy","gk"));
		quiz=new QuizDto("GKQuiz",2);
		quiz.setQuestions(gkQuestions);
	}
	
	@Test
	 void testCreateQuiz() throws Exception{
		Mockito.when(quizLibraryService.createQuiz(any(), eq(List.of(1)))).thenReturn(quiz);
		Mockito.when(quizLibraryService.addQuiz(quiz)).thenReturn(quiz);
		MvcResult mvcResult = mockMvc.perform(post("/addQuiz")
				.param("questionIdList","1"))
				.andExpect(view().name("successQuiz.jsp"))
	            .andExpect(model().attributeExists("message"))
	            .andExpect(model().attribute("message","Quiz added successfully")).andReturn();
    	assertNotNull(mvcResult);
		
	}
	@Test
	 void testCreateQuizWithException() throws Exception{
		Mockito.when(quizLibraryService.createQuiz(any(), eq(List.of(1)))).thenReturn(quiz);
		Mockito.when(quizLibraryService.addQuiz(quiz)).thenThrow(new QuizException("error"));
		MvcResult mvcResult = mockMvc.perform(post("/addQuiz")
				.param("questionIdList","1"))
				.andExpect(view().name("errorQuiz.jsp"))
	            .andExpect(model().attributeExists("message"))
	            .andExpect(model().attribute("message","error")).andReturn();
    	assertNotNull(mvcResult);
	}
	@Test
	 void testDeleteQuiz() throws Exception {
		Mockito.doNothing().when(quizLibraryService).removeQuiz(1);
		MvcResult mvcResult = mockMvc.perform(get("/deleteQuiz")
				.param("id","1"))
			.andExpect(status().isOk())
			.andExpect(view().name("viewQuizzes"))
            .andExpect(model().attributeExists("message"))
            .andExpect(model().attribute("message","Quiz deleted successfully")).andReturn();
    	assertNotNull(mvcResult);
	}

	@Test
	 void testViewAllQuiz() throws Exception{
		Mockito.when(quizLibraryService.getAllQuizzes()).thenReturn(List.of(quiz));
		MvcResult mvcResult = mockMvc.perform(get("/viewQuizzes"))
				.andExpect(status().isOk())
				.andExpect(view().name("viewQuiz.jsp"))
	            .andExpect(model().attributeExists("quizzes"))
	             .andReturn();
	    	     assertNotNull(mvcResult);
	}
//	@Test
//	 void testViewAllQuizWithException() throws Exception{
//		Mockito.when(quizLibraryService.getAllQuizzes()).thenThrow(new NullPointerException("error"));
//		MvcResult mvcResult = mockMvc.perform(get("/viewQuizzes"))
//				.andExpect(status().isOk())
//				.andExpect(view().name("errorQuiz.jsp"))
//	            .andExpect(model().attributeExists("message"))
//	            .andExpect(model().attribute("message","error"))
//	             .andReturn();
//	    	     assertNotNull(mvcResult);
//	}
	@Test
	 void testModifyQuestion() throws Exception{
		Mockito.when(quizLibraryService.createQuiz(any() ,eq(List.of(1)))).thenReturn(quiz);
		Mockito.when(quizLibraryService.modifyQuiz(quiz)).thenReturn(quiz);
		MvcResult mvcResult = mockMvc.perform(post("/updateQuiz")
				.param("questionIdList","1"))
				.andExpect(view().name("successQuiz.jsp"))
	            .andExpect(model().attributeExists("message"))
	            .andExpect(model().attribute("message","Quiz updated successfully")).andReturn();
    	assertNotNull(mvcResult);
	}
	@Test
	void testGetQuestions() throws Exception {
		Mockito.when(questionLibraryService.getAllQuestions()).thenReturn(questionsDto);
		MvcResult mvcResult = mockMvc.perform(get("/getQuestions"))
				.andExpect(view().name("createQuiz.jsp"))
	            .andExpect(model().attributeExists("questions"))
	            .andReturn();
    	assertNotNull(mvcResult);

	}
	@Test
	void testViewById() throws Exception {
		Mockito.when(quizLibraryService.getQuizById(1)).thenReturn(quiz);
		MvcResult mvcResult = mockMvc.perform(get("/viewQuizById")
				.param("id","1"))
			.andExpect(status().isOk())
			.andExpect(view().name("showQuiz.jsp"))
            .andReturn();
		assertNotNull(mvcResult);
	}
	@Test
	void testViewByIdWithException() throws Exception {
		Mockito.when(quizLibraryService.getQuizById(1)).thenThrow(new QuizException("Error"));
		MvcResult mvcResult = mockMvc.perform(get("/viewQuizById")
				.param("id","1"))
			.andExpect(status().isOk())
			.andExpect(view().name("viewQuiz.jsp"))
            .andExpect(model().attribute("message","Error")).andReturn();
		assertNotNull(mvcResult);
	}
	
	@Test
	void modifyQuizToGetDetailsWithException() throws Exception {
		Mockito.when(quizLibraryService.getQuizById(1)).thenThrow(new QuizException("Error"));
		MvcResult mvcResult = mockMvc.perform(get("/modifyQuiz")
				.param("id","1"))
			.andExpect(status().isOk())
			.andExpect(view().name("errorQuiz.jsp"))
            .andReturn();
		assertNotNull(mvcResult);
	}
	
	@Test
	void testModifyQuestionToGetDetails() throws Exception {
		Mockito.when(quizLibraryService.getQuizById(1)).thenReturn(quiz);
		MvcResult mvcResult = mockMvc.perform(get("/modifyQuiz")
				.param("id","1"))
			.andExpect(status().isOk())
			.andExpect(view().name("modifyQuiz.jsp"))
            .andReturn();
		assertNotNull(mvcResult);
	}
	@Test
	 void testModifyQuestionWithException() throws Exception{
		Mockito.when(quizLibraryService.createQuiz(any() ,eq(List.of(1)))).thenReturn(quiz);
		Mockito.when(quizLibraryService.modifyQuiz(quiz)).thenThrow(new QuizException("Error"));
		MvcResult mvcResult = mockMvc.perform(post("/updateQuiz")
				.param("questionIdList","1"))
				.andExpect(view().name("errorQuiz.jsp"))
	             .andReturn();
   	assertNotNull(mvcResult);
	}
	
}
