package com.epam.controllers;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.epam.dtos.QuizDto;
import com.epam.entities.Question;
import com.epam.exceptions.QuizException;
import com.epam.servicelayer.QuizLibraryService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(controllers=QuizAPI.class,excludeAutoConfiguration= {SecurityAutoConfiguration.class})
 class QuizAPITest {
	@MockBean
	QuizLibraryService quizLibraryService;
	@Autowired
	private MockMvc mockMvc;
	List<Question> gkQuestions;
	QuizDto quizDto,quizDto1;
	@BeforeEach
	public void setUp() {
		gkQuestions=Arrays.asList(new Question("Capital of TN",Arrays.asList("chennai","Hyd"),1,"easy","gk"));
		quizDto=new QuizDto("GKQuiz",2);
		quizDto.setQuestions(gkQuestions);
		quizDto1=new QuizDto("GKQuiz",2);
		quizDto1.setQuestions(gkQuestions);
	}
	
	@Test
	 void viewAllQuizes() throws Exception {
		Mockito.when(quizLibraryService.getAllQuizzes()).thenReturn(List.of(quizDto));
		MvcResult mvcResult = mockMvc.perform(get("/quizes"))
				.andExpect(status().isOk())
	            .andExpect(jsonPath("$[0].title").value("GKQuiz"))
	            .andReturn();
   	assertNotNull(mvcResult);
	}
	@Test
	 void viewQuizByID() throws Exception {
		Mockito.when(quizLibraryService.getQuizById(1)).thenReturn(quizDto);
		MvcResult mvcResult = mockMvc.perform(get("/quizes/{id}",1))
				.andExpect(status().isOk())
	            .andExpect(jsonPath("$.title").value("GKQuiz"))
	            .andReturn();
  	assertNotNull(mvcResult);
	}
	@Test
	 void viewQuizByIDWithException() throws Exception {
		Mockito.when(quizLibraryService.getQuizById(1)).thenThrow(QuizException.class);
		MvcResult mvcResult = mockMvc.perform(get("/quizes/{id}",1))
				.andExpect(status().isOk())
	            .andReturn();
 	assertNotNull(mvcResult);
	}

	@Test
	void testAddQuiz() throws Exception {
		Mockito.when(quizLibraryService.createQuiz(any(),eq(List.of(1)))).thenReturn(quizDto);
		Mockito.when(quizLibraryService.addQuiz(quizDto)).thenReturn(quizDto);
		mockMvc.perform(post("/quizes")
			   .param("questionIdList", "1")
               .contentType(MediaType.APPLICATION_JSON)
               .content(new ObjectMapper().writeValueAsString(quizDto)))
               .andExpect(status().isCreated())
               .andExpect(jsonPath("$.title").value("GKQuiz"));
              
	}
	@Test
	void testAddQuizWithException() throws Exception {
		Mockito.when(quizLibraryService.createQuiz(any(),eq(List.of(1)))).thenReturn(quizDto);
		Mockito.when(quizLibraryService.addQuiz(quizDto)).thenThrow(QuizException.class);
		mockMvc.perform(post("/quizes")
			   .param("questionIdList", "1")
			   .contentType(MediaType.APPLICATION_JSON)
               .content(new ObjectMapper().writeValueAsString(quizDto)))
               .andExpect(status().isOk());
              
	}
	@Test
	void testDeleteQuiz() throws  Exception {
		Mockito.doNothing().when(quizLibraryService).removeQuiz(1);
		mockMvc.perform(delete("/quizes/{id}",1))
                 .andExpect(jsonPath("$"). value("Quiz with id "+1+" deleted Sucessfully"));
		
	}
	
	@Test
	void testModifyQuestion() throws Exception {
		Mockito.when(quizLibraryService.createQuiz(any(),eq(List.of(1)))).thenReturn(quizDto);
		Mockito.when(quizLibraryService.modifyQuiz(quizDto)).thenReturn(quizDto);
		mockMvc.perform(put("/quizes")
			   .param("questionIdList", "1")
               .contentType(MediaType.APPLICATION_JSON)
               .content(new ObjectMapper().writeValueAsString(quizDto)))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.title").value("GKQuiz"));
              
	}
	@Test
	void testModifyQuestionWithException() throws Exception {
		Mockito.when(quizLibraryService.createQuiz(any(),eq(List.of(1)))).thenReturn(quizDto);
		Mockito.when(quizLibraryService.modifyQuiz(quizDto)).thenThrow(QuizException.class);
		mockMvc.perform(put("/quizes")
			   .param("questionIdList", "1")
               .contentType(MediaType.APPLICATION_JSON)
               .content(new ObjectMapper().writeValueAsString(quizDto)))
               .andExpect(status().isOk());
              
	}



}
