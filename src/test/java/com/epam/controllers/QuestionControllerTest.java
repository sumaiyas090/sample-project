package com.epam.controllers;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.epam.dtos.QuestionDto;
import com.epam.exceptions.QuestionException;
import com.epam.exceptions.QuestionException;
import com.epam.exceptions.QuestionException;
import com.epam.servicelayer.QuestionLibraryService;

@WebMvcTest(controllers=QuestionController.class,excludeAutoConfiguration= {SecurityAutoConfiguration.class})
 class QuestionControllerTest {
	@MockBean
	QuestionLibraryService questionLibraryService;
	@Autowired
	private MockMvc mockMvc;

	QuestionDto questionDto;

	@BeforeEach
	public void setUp() {
		
		questionDto=new QuestionDto("sample",List.of("1", "2"),1,"easy","gk");
	}

	
	@Test
	void testAddQuestion() throws Exception {	
		Mockito.when(questionLibraryService.addQuestion(any())).thenReturn(questionDto);
		MvcResult mvcResult = mockMvc.perform(post("/addQuestion"))
				.andExpect(view().name("successQuestion.jsp"))
	            .andExpect(model().attributeExists("message"))
	            .andExpect(model().attribute("message","Question add successfully")).andReturn();
    	assertNotNull(mvcResult);
    	
	}
	@Test
	void testAddQuestion1() throws Exception  {	
		Mockito.when(questionLibraryService.addQuestion(any())).thenThrow(new QuestionException("exception"));
		MvcResult mvcResult = mockMvc.perform(post("/addQuestion")).andExpect(status().isOk())
				.andExpect(view().name("errorQuestion.jsp"))
	            .andExpect(model().attributeExists("message"))
	            .andExpect(model().attribute("message","exception")).andReturn();
		assertNotNull(mvcResult);
	}

	
	@Test
	void testGetAllQuestions() throws Exception {
		List<QuestionDto> data = new ArrayList<>();
		data.add(questionDto);
		Mockito.when(questionLibraryService.getAllQuestions()).thenReturn(data);
		MvcResult mvcResult = mockMvc.perform(get("/viewQuestions"))
				.andExpect(status().isOk())
				.andExpect(view().name("viewQuestion.jsp"))
	            .andExpect(model().attributeExists("questions"))
	            .andExpect(model().attribute("questions",data))
	            .andReturn();
		assertNotNull(mvcResult);
	}
	@Test
	void testGetAllQuestionsWithException() throws Exception {
		List<QuestionDto> questionList=List.of();
		Mockito.when(questionLibraryService.getAllQuestions()).thenReturn(questionList);
		MvcResult mvcResult = mockMvc.perform(get("/viewQuestions"))
				.andExpect(status().isOk())
				.andExpect(view().name("viewQuizzes"))
	            .andExpect(model().attributeExists("message"))
	            .andExpect(model().attribute("message","No Questions in question bank"))
	            .andReturn();
		assertNotNull(mvcResult);
	}
	@Test
	void testdeleteQuestion() throws Exception {
		Mockito.doNothing().when(questionLibraryService).removeQuestion(1);
		MvcResult mvcResult = mockMvc.perform(get("/deleteQuestion")
				.param("questionId","1"))
			.andExpect(status().isOk())
			.andExpect(view().name("viewQuestions"))
            .andExpect(model().attributeExists("message"))
            .andExpect(model().attribute("message","Question 1 deleted successfully")).andReturn();
    	assertNotNull(mvcResult);
	}
	@Test
	void testdeleteQuestionWithException() throws Exception {
		Mockito.doThrow(new QuestionException("Error")).when(questionLibraryService).removeQuestion(1);
		MvcResult mvcResult = mockMvc.perform(get("/deleteQuestion")
				.param("questionId","1"))
			.andExpect(status().isOk())
			.andExpect(view().name("errorQuestion.jsp"))
            .andExpect(model().attributeExists("message"))
            .andExpect(model().attribute("message","Error")).andReturn();
    	assertNotNull(mvcResult);
		
		
	}
	@Test
	void testUpdateQuestion() throws Exception {
		Mockito.when(questionLibraryService.modifyQuestion( any())).thenReturn(questionDto);
		MvcResult mvcResult = mockMvc.perform(post("/updateQuestion")
				.param("questionId","1"))
			.andExpect(status().isOk())
			.andExpect(view().name("successQuestion.jsp"))
            .andExpect(model().attribute("message","Question updated successfully")).andReturn();
		assertNotNull(mvcResult);
	}
	
	@Test
	void testViewById() throws Exception {
		Mockito.when(questionLibraryService.getQuestionById(1)).thenReturn(questionDto);
		MvcResult mvcResult = mockMvc.perform(get("/viewQuestionById")
				.param("questionId","1"))
			.andExpect(status().isOk())
			.andExpect(view().name("showQues.jsp"))
            .andExpect(model().attribute("question",questionDto)).andReturn();
		assertNotNull(mvcResult);
	}
	@Test
	void testViewByIdWithException() throws Exception {
		Mockito.when(questionLibraryService.getQuestionById(1)).thenThrow(new QuestionException("Error"));
		MvcResult mvcResult = mockMvc.perform(get("/viewQuestionById")
				.param("questionId","1"))
			.andExpect(status().isOk())
			.andExpect(view().name("viewQuestion.jsp"))
            .andExpect(model().attribute("message","Error")).andReturn();
		assertNotNull(mvcResult);
	}
	@Test
	void modifyQuestionToGetDetailsWithException() throws Exception {
		Mockito.when(questionLibraryService.getQuestionById(1)).thenThrow(new QuestionException("Error"));
		MvcResult mvcResult = mockMvc.perform(get("/modifyQuestion")
				.param("questionId","1"))
			.andExpect(status().isOk())
			.andExpect(view().name("errorQuestion.jsp"))
            .andReturn();
		assertNotNull(mvcResult);
	}
	
	@Test
	void testModifyQuestionToGetDetails() throws Exception {
		Mockito.when(questionLibraryService.getQuestionById(1)).thenReturn(questionDto);
		MvcResult mvcResult = mockMvc.perform(get("/modifyQuestion")
				.param("questionId","1"))
			.andExpect(status().isOk())
			.andExpect(view().name("modifyQuestion.jsp"))
            .andReturn();
		assertNotNull(mvcResult);
	}
	@Test
	void testUpdateQuestionWithException() throws Exception {
		Mockito.when(questionLibraryService.modifyQuestion( any())).thenThrow(new QuestionException("Error"));
		MvcResult mvcResult = mockMvc.perform(post("/updateQuestion")
				.param("questionId","1"))
			.andExpect(status().isOk())
			.andExpect(view().name("errorQuestion.jsp"))
            .andReturn();
		assertNotNull(mvcResult);
	}
	
}
