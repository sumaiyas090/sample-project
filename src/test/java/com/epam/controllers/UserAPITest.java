package com.epam.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.dtos.UserDto;
import com.epam.exceptions.UserException;
import com.epam.servicelayer.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(controllers=UserAPI.class,excludeAutoConfiguration= {SecurityAutoConfiguration.class})
 class UserAPITest {
	@MockBean
	UserService userService;
	@Autowired
	private MockMvc mockMvc;
	UserDto userDto;
	@BeforeEach
	public void setUp() {
		userDto=new UserDto("shahrukh","shahrukh@gmail.com","user","Srkian@1995");
	}
	@Test
   void testSetUser() throws Exception {
		Mockito.when(userService.setUser(userDto)).thenReturn(userDto);
		mockMvc.perform(post("/users/signup")
	               .contentType(MediaType.APPLICATION_JSON)
	               .content(new ObjectMapper().writeValueAsString(userDto)))
	               .andExpect(status().isCreated())
	               .andExpect(jsonPath("$.email").value("shahrukh@gmail.com"));
	}
	@Test
	void testSetUserWithException() throws Exception {
		Mockito.when(userService.setUser(userDto)).thenThrow(new UserException(""));
		mockMvc.perform(post("/users/signup")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content(new ObjectMapper().writeValueAsString(userDto)))
		        .andExpect(status().isOk());
		               
		}
	@Test
	void testlogin() throws Exception {
		Mockito.when(userService.validate("shahrukh@gmail.com","Srkian@1995","user")).thenReturn(true);
		mockMvc.perform(post("/users/login")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content(new ObjectMapper().writeValueAsString(userDto)))
		        .andExpect(status().isOk());
		               
		}
	@Test
	void testloginfail() throws Exception {
		Mockito.when(userService.validate("shahrukh@gmail.com","Srkian@1995","user")).thenReturn(false);
		mockMvc.perform(post("/users/login")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content(new ObjectMapper().writeValueAsString(userDto)))
		        .andExpect(status().isUnauthorized());
		               
		}
	

}
