package com.epam.controllers;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.epam.dtos.QuestionDto;
import com.epam.entities.Question;
import com.epam.exceptions.QuestionException;
import com.epam.servicelayer.QuestionLibraryService;
import com.fasterxml.jackson.databind.ObjectMapper;


@WebMvcTest(controllers=QuestionAPI.class,excludeAutoConfiguration= {SecurityAutoConfiguration.class})
class QuestionApiTest {
	@MockBean
	QuestionLibraryService questionLibraryService;
	@Autowired
	private MockMvc mockMvc;
	Question question;
	QuestionDto questionDto;

	@BeforeEach
	public void setUp() {
		question=new Question("sample",List.of("1", "2"),1,"easy","gk");
		questionDto=new QuestionDto("sample",List.of("1", "2"),1,"easy","gk");
	}

	@Test
	 void viewAllQuestions() throws Exception {
		Mockito.when(questionLibraryService.getAllQuestions()).thenReturn(List.of(questionDto));
		MvcResult mvcResult = mockMvc.perform(get("/questions"))
				.andExpect(status().isOk())
	            .andExpect(jsonPath("$[0].questionTitle").value("sample"))
	            .andReturn();
    	assertNotNull(mvcResult);
	}
	
	
	@Test
	 void viewQuestionByID() throws Exception {
		Mockito.when(questionLibraryService.getQuestionById(1)).thenReturn(questionDto);
		MvcResult mvcResult = mockMvc.perform(get("/questions/{questionId}",1))
				.andExpect(status().isOk())
	            .andExpect(jsonPath("$.questionTitle").value("sample"))
	            .andReturn();
   	assertNotNull(mvcResult);
	}
	@Test
	 void testViewQuestionByIDWithException() throws Exception {
		Mockito.when(questionLibraryService.getQuestionById(1)).thenThrow(QuestionException.class);
		MvcResult mvcResult = mockMvc.perform(get("/questions/{questionId}",1))
				.andExpect(status().isOk())
	            .andReturn();
  	assertNotNull(mvcResult);
	}
	
	@Test
	void testAddQuestions() throws Exception {
		Mockito.when(questionLibraryService.addQuestion(questionDto)).thenReturn(questionDto);
		mockMvc.perform(post("/questions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(questionDto)))
                .andExpect(status().isCreated())
               .andExpect(jsonPath("$.questionTitle"). value("sample"));
               
	}
	@Test
	void testAddQuestionWithException() throws Exception {
		Mockito.when(questionLibraryService.addQuestion(questionDto)).thenThrow(QuestionException.class);
		mockMvc.perform(post("/questions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(questionDto)))
                .andExpect(status().isOk());
               
	}
	@Test
	void testDeleteQuestion() throws  Exception {
		Mockito.doNothing().when(questionLibraryService).removeQuestion(1);
		mockMvc.perform(delete("/questions/{questionId}",1))
		.andExpect(status().isNoContent());
        
		
	}
	@Test
	void testDeleteQuestionWithException() throws  Exception {
		Mockito.doThrow(new QuestionException("error")).when(questionLibraryService).removeQuestion(1);
		mockMvc.perform(delete("/questions/{questionId}",1))
		.andExpect(status().isOk());
		
	}
	@Test
	void testModifyQuestion() throws Exception {
		Mockito.when(questionLibraryService.modifyQuestion(questionDto)).thenReturn(questionDto);
		mockMvc.perform(put("/questions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(questionDto)))
                .andExpect(status().isOk())
               .andExpect(jsonPath("$.questionTitle"). value("sample"));
               
	}
	@Test
	void testModifyQuestionWithException() throws Exception {
		Mockito.when(questionLibraryService.modifyQuestion(questionDto)).thenThrow(QuestionException.class);
		mockMvc.perform(put("/questions")
		.contentType(MediaType.APPLICATION_JSON)
        .content(new ObjectMapper().writeValueAsString(questionDto)))
        .andExpect(status().isOk());     
               
	}


}
